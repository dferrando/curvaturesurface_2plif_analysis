from basicFunctions import *
from vofTo2pLIF import *

imageDir = "/data2/ferrandd/Curvature_Interface_Analysis/VOF/vof_raw_images/averagedTimes/"
kMin = -8.5e5
kMax = 8.5e5
deltaX = 1.27e-6
n_bins = 1000

times = readAllFiles(imageDir)

k_bins = [kMin + i * (kMax - kMin) / n_bins for i in range(n_bins + 1)]
scd_total = pd.DataFrame([[k, 0] for k in k_bins[:-1]], columns = ["k", "s"])

for t in times[1:]:
    print("Processing image: %s" %t)
    image = readImage(imageDir + t)
    image = normalizeImage(image)
    sigma = gradientMagnitude(image, deltaX)
    kappa = curvatureNormalVector(image, deltaX)

    scd_t = surfaceCurvatureDistribution(sigma, kappa, image, deltaX,
                                         kMin = kMin, kMax = kMax,
                                         nBins = n_bins, filterAlpha = True,
                                         filterSigma = True)
    scd_total["s"] += scd_t["s"]

scd_total.to_csv("scd_vof.csv")
dsd = dropSizeDistribution(scd_total, 0, 70e-6, 70, deltaX)
dsd.to_csv("dsd_vof.csv")
