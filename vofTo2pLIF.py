import pandas as pd
import numpy as np
import matplotlib.image as img
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from basicFunctions import *

def fromStringToFloat(l):
    """
    Convert from a string list to a float list
    """
    return [float(listData) for listData in l]

def readRawSlice(raw, var = "phi"):
    """
    Read .raw data from OpenFoam and transform it to a pandas DataFrame
    """

    with open(raw, "r") as data:
        data = data.read().splitlines()
        data = [coor.split(' ') for coor in data[2:]]
        data = [fromStringToFloat(stringList) for stringList in data]
    return pd.DataFrame(data, columns=['x', 'y', 'z', '%s' %var])

def interpolationToRegularGrid(data, var, xMin, xMax, yMin, yMax,
                axes = ['x','y'], nx = 100j, ny = 100j,
                interpolation='nearest'):
    """
    Create a new grid and interpolate the data into it
    """
        
    x, y = np.mgrid[xMin:xMax:nx, yMin:yMax:ny]

    return griddata(data[axes].values, data[var].values,
                     (x, y), method = interpolation).T

def saveSliceImage(data, saveDir, colormap = plt.cm.gray, vmin = 0, vmax = 1):
    """
    Save a image from an openfoam slice
    """
    img.imsave(saveDir, data, cmap = colormap, vmin = vmin, vmax = vmax)

if __name__ == '__main__':

    mainDir = "/data3/ferrandd/CRSB_Simplex_Swirl_Atomizer/externalSimulation/limitedLinear/interfoam_externalSector_298K_1bar/postProcessing/pLIF/"
    processedDir = "/data2/ferrandd/Curvature_Interface_Analysis/VOF/vof_raw_images/"
    times = sorted(readAllFiles(mainDir), key =float)

    slices = ["a0", "a2_5", "a5_0", "a7_5", "a10_0", "a12_5", "a15_0",
              "a17_5", "a20_0", "b2_5", "b5_0", "b7_5", "b10_0", "b12_5",
              "b15_0", "b17_5", "b20_0"]
   
    for t in times:
        print("Working with data from time: %.2e" %float(t))
        timeDir = processedDir + "averagedTimes/" 
        total_grid = np.zeros((1024, 1024))

        for s in slices:
            if s[0] == "a":
                angle = s[1:]
            elif s[0] == "b":
                angle = "-" + s[1:]
            sliceDir = processedDir + "times/" + "sliceAngle_" + angle
            makeDir(sliceDir)

            raw = readRawSlice(mainDir + t + "/alpha.fuel_%s.raw" %s, 
                    var = "alpha")                     
            raw["r"] = (raw["x"]**2 + raw["y"]**2)**0.5
            grid = interpolationToRegularGrid(raw,"alpha",xMin=0, xMax=0.0013,
                                              yMin=0,yMax=0.0013,axes=['r','z'],
                                              nx=1024j, ny=1024j)
            saveSliceImage(grid, "%s/%.8f.png" %(sliceDir, float(t)))
            total_grid += grid 

        saveSliceImage(total_grid, "%s%.8f.png" %(timeDir, float(t)),
                vmax = total_grid.max())
